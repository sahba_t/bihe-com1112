/*******************************************************************************
 * Name: Sahba Tashakori
 * Date: 05/04/2012
 * Description: This is a small shopping cart application that allows the user to order a
 *              number of fixed items from the command prompt, calculates the final
 *              price and returns the total based on the available credit line.
 ******************************************************************************/
package org.bihe.com1112.utility;

import org.bihe.com1112.entity.Customer;
import org.bihe.com1112.entity.Product;

public final class Transaction {
    private final Customer customer;
    private String[]       orders;
    private Product[]      productList = null;
    private double         netCost;
    private final String   order;

    public Transaction(final Customer customer, final String order) {
        this.customer = customer;
        this.order = order;
        this.orders = order.split(" ");
    }

    public char finalizeTransaction() {
        calculateNetCost();

        char result = 'n';
        final StringBuilder messageBuilder = new StringBuilder();
        if (this.customer.hasEnoughCreditForCost(this.netCost)) {
            messageBuilder.append(this.customer.getName());
            messageBuilder.append(" - (");
            messageBuilder.append(this.productList == null ? "null" : this.order);
            messageBuilder.append(") - ");
            messageBuilder.append(this.netCost * 10);
            messageBuilder.append("R");
            System.out.println(messageBuilder.toString());
            System.out.print("If you want to start over press 'y'. If you want to quit press 'q': ");

        } else {
            System.out.println("Not enough credit for the net cost " + this.netCost);
            System.out.print("Press 'c' to enter another list or 'Q' to exit and 'y' start over: ");
        }

        result = SavitchIn.readLineNonwhiteChar();
        return result;
    }

    public boolean isValidTransaction() {
        boolean result = true;
        while ((this.orders.length < 2 || this.orders.length % 2 != 0) && result) {
            System.out.print("Invalid transaction (inputs shorter than desired).\nDo you want to enter a new one (y/n)? ");
            final char choice = SavitchIn.readLineNonwhiteChar();
            if (choice == 'y' || choice == 'Y') {
                System.out.println("Please enter a new order");
                this.orders = SavitchIn.readLine().trim().split(" ");
            } else
                result = false;
        }
        if (result) {
            this.productList = new Product[this.orders.length / 2];
            for (int i = 0, j = 0; j < this.productList.length; i += 2, j++)
                this.productList[j] = new Product(this.orders[i].charAt(0),
                        Integer.valueOf(this.orders[i + 1]));

        }
        return result;

    }

    private void calculateNetCost() {
        this.netCost = 0;
        if (this.productList != null)
            for (final Product element : this.productList)
                this.netCost += element.calculateNetPrice();
    }

}
