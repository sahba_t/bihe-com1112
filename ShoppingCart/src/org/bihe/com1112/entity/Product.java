/*******************************************************************************
 * Name: Sahba Tashakori
 * Date: 05/04/2012
 * Description: This is a small shopping cart application that allows the user to order a
 *              number of fixed items from the command prompt, calculates the final
 *              price and returns the total based on the available credit line.
 ******************************************************************************/
package org.bihe.com1112.entity;

public class Product {

    private static String generateProductSerial(final char productChar) {
        final StringBuilder serialBuilder = new StringBuilder();
        serialBuilder.append(productChar);
        serialBuilder.append(Math.random() * 9);
        serialBuilder.append(Math.random() * 9);
        return serialBuilder.toString();
    }
    private final String SERIAL;

    private final int PRODUCT_PRICE;

    private final int quantity;

    public Product(final char productChar, final int quantity) {
        this.quantity = Math.abs(quantity);
        switch (productChar) {
        case 'm':
        case 'M':
            this.PRODUCT_PRICE = 750;
            this.SERIAL = Product.generateProductSerial(productChar);
            break;
        case 'D':
        case 'd':
            this.PRODUCT_PRICE = 800;
            this.SERIAL = Product.generateProductSerial(productChar);
            break;
        case 'C':
        case 'c':
            this.PRODUCT_PRICE = 2500;
            this.SERIAL = Product.generateProductSerial(productChar);
            break;
        default:
            this.PRODUCT_PRICE = 0;
            this.SERIAL = "Unavailable";
            break;

        }

    }

    public double calculateNetPrice( ) {
        double netPrice;
        if (this.PRODUCT_PRICE != 0)
            netPrice = this.quantity * this.PRODUCT_PRICE;
        else
            netPrice = 0;
        return netPrice;
    }

    public final int getProductPrice() {
        return this.PRODUCT_PRICE;
    }

    public String getSerial() {
        return this.SERIAL;
    }


}
