/*******************************************************************************
 * Name: Sahba Tashakori
 * Date: 05/04/2012
 * Description: This is a small shopping cart application that allows the user to order a
 *              number of fixed items from the command prompt, calculates the final
 *              price and returns the total based on the available credit line.
 ******************************************************************************/

package org.bihe.com1112.entity;

import java.util.Random;


public class Customer {
    private final double BALANCE;
    private final String NAME;

    public Customer(final String name) {
        this.BALANCE = Math.round(new Random().nextDouble() * 14000 + 1000);

        this.NAME = name;
    }

    public double getBalance() {
        return this.BALANCE;
    }

    public String getName() {
        return this.NAME;
    }

    public boolean hasEnoughCreditForCost(final double cost) {
        return this.BALANCE >= cost;
    }

    @Override
    public String toString() {
        return "Name: " + getName() + ", " + "Balance: " + getBalance() + "R";
    }
}
