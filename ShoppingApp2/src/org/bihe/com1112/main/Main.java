/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description: Main class
 */

package org.bihe.com1112.main;

import org.bihe.com1112.entity.AbstractProduct;
import org.bihe.com1112.entity.Customer;
import org.bihe.com1112.exception.OrderException;
import org.bihe.com1112.exception.TransactionException;
import org.bihe.com1112.util.SavitchIn;
import org.bihe.com1112.util.SortingFactor;
import org.bihe.com1112.util.Transaction;

public class Main {

	private static boolean mustContinue = true;
	private static boolean newCustomer = true;

	public static void main(String[] args) {

		Customer currentCustomer = null;
		while (mustContinue) {
			if (newCustomer) {
				System.out.print("Please enter the customer name:");
				String customerName = SavitchIn.readWord();
				currentCustomer = new Customer(customerName);
				System.out.println(currentCustomer.toString());
			}
			System.out.println("Please enter the order:");
			String orders = SavitchIn.readLine().trim();
			if (orders.equals("") || orders == null) {
				System.out.print("Invalid order!");
				shouldContinue(false);
			} else {
				Transaction transaction = new Transaction(currentCustomer,
						orders);
				try {
					transaction.validateTransaction();
					System.out.println(transaction.finalizeTransaction());
					System.out.println("Please choose one sort Option:");
					System.out
							.println("By serial number(0)/By Name Then Serial Number(1):");
					int sortChoice = SavitchIn.readLineInt();
					SortingFactor sortFactor = null;
					if (sortChoice == 1) {
						sortFactor = SortingFactor.NAME;
					} else {
						sortFactor = SortingFactor.SERIAL_NUMBER;
					}
					if (sortFactor != null) {
						for (AbstractProduct p : transaction
								.getSortedProductList(sortFactor)) {
							System.out.println(p.toString());
						}
					}
					shouldContinue(true);
				} catch (OrderException ex) {
					System.out.println(ex.getMessage());
					shouldContinue(false);
				} catch (TransactionException ex) {
					System.out.println(ex.getMessage());
					shouldContinue(false);
				} catch (NumberFormatException ex) {
					System.out.println("You have entered an invalid option");
					shouldContinue(true);
				}
			}
		}
		System.out.println("The End!");
	}

	private static void shouldContinue(boolean isExitStatement) {
		if (!isExitStatement) {
			System.out.println("Do you want to enter orders once more?y/n?");
			char choice = SavitchIn.readLineNonwhiteChar();
			if (choice == 'y' || choice == 'Y') {
				newCustomer = false;
				mustContinue = true;
			} else {
				shouldContinue(true);
			}
		} else {
			System.out.print("Do you Want to Exit?y/n?");
			char exit = SavitchIn.readLineNonwhiteChar();
			if (exit == 'y' || exit == 'Y') {
				mustContinue = false;

			} else {
				mustContinue = true;
				newCustomer = true;
			}
		}
	}
}
