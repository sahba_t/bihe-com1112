/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description: Used for specifying sorting criteria
 */

package org.bihe.com1112.util;

public enum SortingFactor {
	NAME, SERIAL_NUMBER
}
