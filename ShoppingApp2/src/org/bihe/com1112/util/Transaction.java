/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description: Handles Transaction process
 */

package org.bihe.com1112.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.bihe.com1112.entity.AbstractProduct;
import org.bihe.com1112.entity.Customer;
import org.bihe.com1112.entity.ProductFactory;
import org.bihe.com1112.exception.OrderException;
import org.bihe.com1112.exception.TransactionException;

public class Transaction {

	private final Customer customer;
	private String[] orders;
	private ArrayList<AbstractProduct> productList = null;
	private double netCost;
	private String order;

	public Transaction(Customer customer, String order) {

		this.customer = customer;
		this.order = order;
		this.orders = order.split(" ");
	}

	public void validateTransaction() throws OrderException {
		if (orders.length < 2 || orders.length % 2 != 0) {
			throw new OrderException(
					"Invalid transaction (inputs shorter than desired)");
		}
		productList = new ArrayList<AbstractProduct>(orders.length);
		for (int i = 0, j = 0; j < orders.length / 2; i += 2, j++) {
			ProductFactory.getProduct(orders[i].charAt(0), (orders[i + 1]),
					productList);

		}

		productList.trimToSize();
	}

	private void calculateNetCost() {
		netCost = 0;
		if (productList != null) {
			for (int i = 0; i < productList.size(); i++) {
				netCost += productList.get(i).calculateNetPrice();
			}
		}

	}

	public String finalizeTransaction() throws TransactionException {
		calculateNetCost();
		StringBuilder messageBuilder = new StringBuilder();
		if (customer.checkCrediblity(netCost)) {
			messageBuilder.append(customer.getNAME());
			messageBuilder.append(" - (");
			messageBuilder.append(productList == null ? "null" : order);
			messageBuilder.append(") - ");
			messageBuilder.append(netCost * 10);
			messageBuilder.append("R");
			return messageBuilder.toString();
		} else {

			throw new TransactionException(String.format(
					"Credit Not Enogh! Total Cost: %.2f, balance: %.2f",
					netCost, customer.getBalance()));

		}
	}

	public List<AbstractProduct> getSortedProductList(
			SortingFactor sortingFactor) {
		// Sorting Operation:

		switch (sortingFactor) {
		case NAME:
			Collections.sort(productList, new Comparator<AbstractProduct>() {

				// Anonymous inner class!
				@Override
				public int compare(AbstractProduct o1, AbstractProduct o2) {

					return (o1.PRODUCT_TYPE.compareTo(o2.PRODUCT_TYPE) == 0) ? o1.SERIAL
							.compareTo(o2.SERIAL) : o1.PRODUCT_TYPE
							.compareTo(o2.PRODUCT_TYPE);

				}
			});
			break;
		case SERIAL_NUMBER:
			Collections.sort(productList, new Comparator<AbstractProduct>() {

				@Override
				public int compare(AbstractProduct o1, AbstractProduct o2) {

					return o1.SERIAL.compareTo(o2.SERIAL);
				}
			});
			break;

		default:
			break;
		}
		// The product list is sorted now
		return productList;
	}
}
