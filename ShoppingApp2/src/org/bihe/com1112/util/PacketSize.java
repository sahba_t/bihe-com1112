/**
 * Name: Sahba
 * Date: 26/5/12
 * Description: Used for milk packet size. Demonstrating enumeration capabilities.
 */
package org.bihe.com1112.util;

public enum PacketSize {
	NORMAL("Normal"), BIG("Big"), SMALL("Small");
	private String size;

	private PacketSize(String size) {
		this.size = size;
	}

	public String getPacketSize() {
		return size;
	}
}
