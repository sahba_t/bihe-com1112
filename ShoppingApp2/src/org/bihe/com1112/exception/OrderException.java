/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description: Used for errors in the order formation
 */

package org.bihe.com1112.exception;

public class OrderException extends Exception {

	private static final long serialVersionUID = 1L;

	public OrderException() {
		super();
	}

	public OrderException(String message) {
		super(message);

	}

	public OrderException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);

	}
}
