/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description: Used for errors in the transaction process
 */

package org.bihe.com1112.exception;

/**
 * 
 * @author Sahba
 */
public class TransactionException extends Exception {

	private static final long serialVersionUID = 1L;

	public TransactionException() {
	}

	public TransactionException(String message) {
		super(message);
	}
}
