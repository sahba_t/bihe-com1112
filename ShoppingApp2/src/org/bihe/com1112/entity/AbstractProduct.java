/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description:Abstract representation of products
 */

package org.bihe.com1112.entity;

public abstract class AbstractProduct {
	public final String SERIAL;
	protected final int PRODUCT_PRICE;
	protected final int quantity;
	public final String PRODUCT_TYPE;

	public AbstractProduct(int price, int quantity, String serial,
			String ProductType) {
		this.quantity = Math.abs(quantity);
		this.PRODUCT_PRICE = price;
		this.SERIAL = serial;
		this.PRODUCT_TYPE = ProductType;
	}

	public double calculateNetPrice() {
		double netPrice;
		if (PRODUCT_PRICE != 0)
			netPrice = quantity * PRODUCT_PRICE;
		else
			netPrice = 0;
		return netPrice;
	}

	@Override
	public String toString() {
		return "PRODUCT_TYPE: =" + PRODUCT_TYPE + ", SERIAL: " + SERIAL;
	}

}
