/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description: Simulates a product category
 */
package org.bihe.com1112.entity;

import org.bihe.com1112.util.PacketSize;

public class Milk extends AbstractProduct {

	private final static PacketSize[] AVALAIBLE_SIZES = PacketSize.values();
	public final PacketSize SIZE;

	public Milk(int milkprice, int quantity, String serial, String productType) {
		super(milkprice, quantity, serial, productType);
		SIZE = AVALAIBLE_SIZES[(int) Math.random() * AVALAIBLE_SIZES.length];

	}

}
