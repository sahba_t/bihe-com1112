/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description: Simulates a product category
 */

package org.bihe.com1112.entity;

public class Can extends AbstractProduct {
	private static final String[] AVAILABLE_COMPANYIES = { "CompanyA",
			"CompanyB", "CompanyC", "CompanyD" };
	public final String MANUFACTURER;

	public Can(int canPrice, int quantity, String ProductSerial,
			String productType) {
		super(canPrice, quantity, ProductSerial, productType);
		MANUFACTURER = AVAILABLE_COMPANYIES[(int) (Math.random() * 4)];
	}

}
