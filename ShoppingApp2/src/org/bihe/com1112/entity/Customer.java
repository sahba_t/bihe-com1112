/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description: Simulates a customer
 */
package org.bihe.com1112.entity;

import java.util.Random;

public class Customer {

	private double balance;
	private final String NAME;

	public Customer(String name) {
		balance = (new Random().nextDouble() * 14000 + 1000);
		NAME = name;
	}

	public double getBalance() {
		return balance;
	}

	public String getNAME() {
		return NAME;
	}

	public boolean checkCrediblity(double cost) {
		return balance >= cost;
	}

	@Override
	public String toString() {
		return "Name: " + getNAME() + ", "
				+ String.format("balance: %.2f", getBalance());
	}
}
