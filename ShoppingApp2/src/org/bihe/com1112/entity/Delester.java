/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description: Simulates a product category
 */
package org.bihe.com1112.entity;

import java.util.GregorianCalendar;

public class Delester extends AbstractProduct {
	private final GregorianCalendar EXPIRATION_DATE = new GregorianCalendar();

	public Delester(int delesterPrice, int quantity, String productSerial,
			String productType) {
		super(delesterPrice, quantity, productSerial, productType);
		// Let's give the Delester 6 months interval
		int candidateDate;
		candidateDate = EXPIRATION_DATE.get(GregorianCalendar.MONTH) + 6;
		if (candidateDate > 11) {
			EXPIRATION_DATE.set(GregorianCalendar.YEAR,
					EXPIRATION_DATE.get(GregorianCalendar.YEAR) + 1);
			candidateDate -= 11;
		}
		EXPIRATION_DATE.set(GregorianCalendar.MONTH, candidateDate);
	}
}
