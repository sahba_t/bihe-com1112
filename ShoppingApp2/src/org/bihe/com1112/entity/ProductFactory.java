/**
 * Name: Sahba
 * Date: June 4, 2012
 * Description: Decides which product category should be used
 * for the given order 
 */

package org.bihe.com1112.entity;

import java.util.List;
import org.bihe.com1112.exception.OrderException;

public final class ProductFactory {

	private static final int MilkPrice = 750;
	private static final int CanPrice = 2500;
	private static final int DelesterPrice = 800;

	public static void getProduct(final char code, final String QUANTITY,
			List<AbstractProduct> products) throws OrderException {

		int quantity;
		try {
			quantity = Integer.parseInt(QUANTITY);
		} catch (NumberFormatException e) {
			throw new OrderException(
					"You should have enterted a number instead of: " + QUANTITY);
		}
		for (int i = 0; i < quantity; i++) {
			products.add(MakeProduct(code, 1));
		}

	}

	private static AbstractProduct MakeProduct(char code, int quantity)
			throws OrderException {
		AbstractProduct product = null;

		switch (code) {
		case 'm':
		case 'M':
			product = new Milk(MilkPrice, quantity,
					generateProductSerial(code), "Milk");
			break;
		case 'D':
		case 'd':
			product = new Delester(DelesterPrice, quantity,
					generateProductSerial(code), "Delester");
			break;
		case 'C':
		case 'c':
			product = new Can(CanPrice, quantity, generateProductSerial(code),
					"Can");

			break;
		default:
			throw new OrderException("Invalid Character Found: " + code);

		}
		return product;
	}

	private static String generateProductSerial(char productChar) {
		StringBuilder serialBuilder = new StringBuilder();
		serialBuilder.append(productChar);
		serialBuilder.append(String.format("%.0", Math.random() * 9));
		serialBuilder.append(String.format("%.0", Math.random() * 9));
		return serialBuilder.toString();
	}
}