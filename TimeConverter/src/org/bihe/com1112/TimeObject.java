/*
 * Name: Misagh Moayyed
 * Date: January 10th 2010
 * Assignment: Write a small Java program that converts from 24-hour time to 12-hour time format. 
 */
package org.bihe.com1112;

public class TimeObject {
	private long hour;
	private final long minute;
	private String ampm;

	/*
	 * Overloaded constructor. It tries to convert 24-hour format time to
	 * 12-hour format
	 */
	public TimeObject(final long hour, final long minute) {
		if (hour > 12) {
			this.hour = hour - 12;
			ampm = "PM";
		} else {
			this.hour = hour;
			ampm = "AM";
		}
		this.minute = minute;
	}

	/*
	 * Here, we have overridden the toString() method to simply return the
	 * formatted String. This method is used by the Main class
	 * 
	 * Instead of building the String manually, we will use the StringBuilder
	 * class just to make garbage collection happy!
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append(getHour());
		builder.append(":");
		builder.append(getMinute());
		builder.append(" ");
		builder.append(getAmPm());
		return builder.toString();
	}

	/*
	 * Notice how all the getXYZ() methods are private. They're only used by
	 * this class and nobody outside needs to know about them. So why make them
	 * public ?!
	 */
	private String getAmPm() {
		return this.ampm;
	}

	private long getHour() {
		return this.hour;
	}

	private long getMinute() {
		return this.minute;
	}

}
