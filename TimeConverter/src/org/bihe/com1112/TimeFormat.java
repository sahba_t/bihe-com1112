/*
 * Name: Misagh Moayyed
 * Date: January 10th 2010
 * Assignment: Write a small Java program that converts from 24-hour time to 12-hour time format. 
 */
package org.bihe.com1112;

public class TimeFormat {

	/*
	 * Parses the input string and returns a TimeObject object that contains the
	 * formatted time input The only exception this method throws is
	 * InvalidTimeFormatException
	 */
	public TimeObject parseTime(final String input) throws InvalidTimeFormatException {
		TimeObject time = null;
		try {

			/*
			 * The rules of WHEN exceptions are thrown are specified in the
			 * assignment. The valid format is HH:MM
			 */
			if (input == null || input.trim().length() == 0)
				throw new IllegalArgumentException("Input cannot be blank");

			final int colonIndex = input.indexOf(":");
			if (colonIndex == -1)
				throw new IllegalArgumentException("Invalid time format. There`s no time as " + input);

			final String hour = input.substring(0, colonIndex).trim();
			final long parsedHour = getLongFromString(hour);
			if (parsedHour < 0 || parsedHour > 24)
				throw new InvalidHourFormatException("Invalid hour format. There`s no time as " + input);

			final String minute = input.substring(colonIndex + 1).trim();
			final long parsedMinute = getLongFromString(minute);
			if (parsedMinute < 0 || parsedMinute > 60)
				throw new InvalidMinuteFormatException("Invalid minute format. There`s no time as " + input);

			// Return the time object here...
			time = new TimeObject(parsedHour, parsedMinute);
		} catch (final IllegalArgumentException ex) {

			/*
			 * All other exceptions are a child of InvalidTimeFormatException,
			 * so we can easily throw them as part of
			 * InvalidTimeFormatException. But IllegalArgumentException is not a
			 * child of InvalidTimeFormatException. So, we must wrap its message
			 * around the InvalidTimeFormatException and then throw that again!
			 */
			throw new InvalidTimeFormatException(ex.getMessage());
		}
		return time;
	}

	/*
	 * Parse a given input string to a long value. Returns -1 if the input
	 * cannot be parsed.
	 */
	private long getLongFromString(final String str) {
		long value = -1;
		try {
			value = Long.parseLong(str);
		} catch (final NumberFormatException e) {
			// do nothing here! value is still assigned to -1
		}
		return value;
	}

}
