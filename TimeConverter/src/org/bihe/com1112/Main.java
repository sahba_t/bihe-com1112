/*
 * Name: Misagh Moayyed
 * Date: January 10th 2010
 * Assignment: Write a small Java program that converts from 24-hour time to 12-hour time format. 
 */
package org.bihe.com1112;

public class Main {

	public static void main(final String[] args) {
		String again = "y";
		final TimeFormat formatter = new TimeFormat();

		/*
		 * Continue while the again String is y or Y. We initially set the value
		 * to y so we can enter the loop. At the end of the loop, we ask the
		 * user again and assign the value to the again string.
		 */
		while (again.equalsIgnoreCase("y")) {
			System.out.println("Enter time in 24-hour format:");
			final String line = SavitchIn.readLine();

			/*
			 * You may notice that the only exception we catch here is
			 * InvalidTimeFormatException
			 */
			try {
				final TimeObject time = formatter.parseTime(line);
				System.out.println("That is the same as");

				/*
				 * This is where we call the toString() method that is
				 * overridden in the TimeObject class.
				 */
				System.out.println(time.toString());
			} catch (final InvalidTimeFormatException e) {
				System.out.println(e.getMessage());
			}

			// Ask the user if he wishes to continue...
			System.out.println("Again? (y/n)");
			again = String.valueOf(SavitchIn.readLineWord());
		}

		// Always display this message at the end. We could somehow also do this
		// with a finally block
		System.out.println("End of program");
	}

}
