/*
 * Name: Misagh Moayyed
 * Date: January 10th 2010
 * Assignment: Write a small Java program that converts from 24-hour time to 12-hour time format. 
 */
package org.bihe.com1112;

public class InvalidHourFormatException extends InvalidTimeFormatException {

	private static final long serialVersionUID = 1L;

	public InvalidHourFormatException(final String msg) {
		super(msg);
	}

}
